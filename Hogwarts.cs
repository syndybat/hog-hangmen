using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;

namespace ConsoleApplication1
{
    
    class Hogwarts
    {
        string hog = "http://www.hogwarts.cz/index.php";
        string session;

        public void run()
        {
            session = loginRequest();
            Hangman hangman = new Hangman(this.session);
            hangman.main();

        }
        public string loginRequest()
        {
            WebRequest request = WebRequest.Create(this.hog);

            var postData = "akce=login";
            postData += "&prihljmeno=username";
            postData += "&heslo=password";
            var data = Encoding.ASCII.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;

            var dataStream = request.GetRequestStream();
            dataStream.Write(data, 0, data.Length);

            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            int index = responseString.IndexOf("session=");
            int lenght = responseString.IndexOf('"',index+8)-index-8;
            string session = responseString.Substring(index + 8, lenght);
            dataStream.Close();
            response.Close();
            
            return session;
        }
        public void logoutRequest()
        {

            WebRequest request = WebRequest.Create(this.hog + "?akce=logout&session=" + this.session);
            WebResponse response = request.GetResponse();
            response.Close();
            return;
        }



    }
}
