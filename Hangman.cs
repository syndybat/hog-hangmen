﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

namespace ConsoleApplication1
{

   
    class Hangman
    {
        string session;
        string hangman = "http://www.hogwarts.cz/prasinky/hangman1.php?"; //?akce=hra
        string hangmanLetter = "http://www.hogwarts.cz/prasinky/hangman1.php?letter=";
        HashSet<string> words = new HashSet<string>();
        List<string> tempWords = new List<string>();
        string usedLetters="";
        int addedWords = 1;
        int pocetHer = 0;

        public Hangman(String session)
        {
            this.session = session;
            this.deserializuj();
        }
        public void main()
        {
            Console.WriteLine("Vítejte v ďáblově jámě");
            while(true){
                Console.WriteLine("co to bude?");
                Console.WriteLine("1-hra");
                Console.WriteLine("2-statika");
                Console.WriteLine("3-výpis databáze slov");
                Console.WriteLine("4-konec");
                switch (Console.ReadKey().KeyChar)
                {
                    case '1':
                        play();
                        break;
                    case '2':
                        Console.WriteLine("Počet slov v databázi: " + this.words.Count);
                        break;
                    case '3':
                        int i = 0;
                        while (i < this.words.Count)
                        {
                            Console.WriteLine(i + ". " + this.words.ElementAt(i));
                            i++;
                        }
                        break;
                    case '4':
                        return;
                }
            }
            
        }
        public void play()
        {
            string word;
            char letter='0';
            string response;
            bool isInDictionary = true;
            int mistakes = 0;
            int waitTime = 0;
            int terminate = 50;
            Random random = new Random();

            while (true)
            {
                if (Console.KeyAvailable)
                {
                    if (Console.ReadKey().KeyChar == '9')
                    {
                        break;
                    }
                }
               this.pocetHer++;
               Console.WriteLine("New request hra {0}",pocetHer);
               response = this.getRequest(this.hangman, this.session);
               Thread.Sleep(waitTime);

                word = GetHangmanWord(response);
                
                while (Console.KeyAvailable)
                {
                    Console.ReadKey(true);
                }
                 while (word != null)
                {
                    if (word.Length <= 10)
                    {
                        waitTime = 2000;
                    }
                    else if(word.Length <= 18)
                    {
                        waitTime = 1300;
                    }
                    else
                    {
                        waitTime = 1000;
                    }
                    if (this.usedLetters.Length <= 4)
                    {
                        waitTime -= random.Next(40,80);
                    }
                    else
                    {
                        waitTime += random.Next(50, 90);
                    }
                    waitTime += random.Next(-120, 200);
                    Thread.Sleep(waitTime);

                    Console.WriteLine(DateTime.Now.ToString("H:mm:ss") +": "+"Slovo: " + word + ", chyb: " + mistakes+ ", used: "+this.usedLetters);
                    if (word.Contains('_'))

                    {
                        
                        if (isInDictionary)
                        {
                            this.fillTempWords(word); //change name!!! not always fill
                            letter = this.guessLetter();
                        }
                        if (!isInDictionary || letter == '0')
                        {
                            Console.Write("Hadej ty: ");
                            isInDictionary = false;
                            do {
                                letter = Console.ReadKey().KeyChar;
                                Console.WriteLine();
                                if (this.usedLetters.Contains(letter))
                                {
                                    Console.WriteLine("toto pismeno uz bylo pouzito zkus to znova");
                                    letter = '0';
                                }
                                if (!((letter >= 'a' && letter <= 'z') || (letter >= 'A' && letter <= 'Z'))){
                                    Console.WriteLine("nezadal jsi platny znak");
                                    letter = '0';
                                }
                            } while (letter == '0');
                        }
                        
                        if (letter != '0')
                        {
                                    //Console.WriteLine("hadam "+letter);
                          response = this.getRequest(this.hangmanLetter + letter, this.session);
                            this.usedLetters += letter;
                          word = GetHangmanWord(response);
                            log(word, 4);
                            if (word != null)
                            {
                                if (!word.Contains(this.usedLetters.Last()))
                                {
                                    mistakes++;
                                }
                            }
                            if (random.Next(0, 10) >= 7)
                            {
                                Thread.Sleep(random.Next(100,250));
                            }
                        }
                       

                    }else
                    {
                        if (!isInDictionary)
                        {
                            this.words.Add(word);
                            this.addedWords++;
                            log(word, 2);
                        }
                        else
                        {
                            log(word, 1);
                        }
                        break;
                    }
                }
                //clean
                mistakes = 0;
                isInDictionary = true;
                this.tempWords.Clear();
                this.usedLetters = "";
                Thread.Sleep(random.Next(1000,4000));
                if (this.addedWords % 10 ==0)
                {
                    addedWords++;
                    this.serializuj();
                }
                if(this.pocetHer % terminate==0)
                {
                    Console.WriteLine("Chcete pokracovat?");
                    if (Console.ReadKey().KeyChar != 'y')
                    {
                        break;
                    }
                }
            }
            if (this.addedWords % 10 != 0)
            {
                this.serializuj();
            }
            while (Console.KeyAvailable)
            {
                Console.ReadKey(true);
            }
        }

        protected void fillTempWords(string word)
        {
            if (this.tempWords.Count == 0)
            {
                foreach (string element in this.words.Where<string>(str => str.Length == word.Length))
                {
                    this.tempWords.Add(element);
                    /* Boolean match = true;
                     for (int i = 0; i < word.Length; i++)
                     {
                         if (!(word[i] == element[i] || word[i] == '_'))
                         {
                             match = false;
                             break;
                         }
                     }
                     if (match)
                     {
                         this.tempWords.Add(element);
                     }*/
                }
            }
            else
            {
                List<string> removeList = new List<string>();
                foreach (string element in this.tempWords)
                {
                    if ((this.usedLetters.Last() >= 'a' && this.usedLetters.Last() <= 'z') || (this.usedLetters.Last() >= 'A' && this.usedLetters.Last() <= 'Z'))
                    {
                        if (!word.Contains(this.usedLetters.Last()) && element.Contains(this.usedLetters.Last()))
                        {
                            removeList.Add(element);
                            continue;
                        }
                    }
                    
                    for (int i = 0; i < word.Length; i++)
                    {
                        if (!(word[i] == element[i] || word[i] == '_'))
                        {
                            removeList.Add(element);  
                        }
                    }
                    
                }
                foreach (string element in removeList)
                {
                    tempWords.Remove(element);
                }
            }
        }

        protected char guessLetter()
        {
            char letter;
            var alphabet = new Dictionary<char, int>();


            
                for (char i = 'a', l = 'z'; i <= l; i++)
                {
                    alphabet.Add(i, tempWords.Where<string>(str => str.Contains(i)).Count());
                }

                alphabet = alphabet.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);

            while (true)
            {   
                if (alphabet.FirstOrDefault().Value == 0)
                {
                    return '0';
                }
                letter = alphabet.FirstOrDefault().Key;
                if (!this.usedLetters.Contains(letter))
                {
                    break;
                }
                alphabet.Remove(letter);
            }

            
            return letter;
        }

        

        public string getRequest(string url, string session)
        {

            WebResponse response = null;
            
            do {
                try
                {
                    WebRequest request = WebRequest.Create(url + "&session=" + session);
                    response = request.GetResponse();
                }
                catch
                {
                    Thread.Sleep(750);
                    Console.WriteLine("bad respons repeated");
                }
            } while (response == null);
            
            
            // Display the status.
           // Console.WriteLine(((HttpWebResponse)response).StatusDescription);
            // Get the stream containing content returned by the server.
            Stream dataStream = response.GetResponseStream();
            // Open the stream using a StreamReader for easy access.
            StreamReader reader = new StreamReader(dataStream);
            // Read the content.
            string responseFromServer = reader.ReadToEnd();
            // Clean up the streams and the response.
            reader.Close();
            response.Close();
            return responseFromServer;
        }
        protected string eraseWhiteSpace(string str)
        {
            StringBuilder sb = new StringBuilder(str.Length);
            for (int i = 0; i < str.Length; i++)
            {
                char c = str[i];
                if (!char.IsWhiteSpace(c))
                    sb.Append(c);
            }
            return sb.ToString();
        }
        private string GetHangmanWord(string htmlString)
        {
            String failedString = "slovo bylo:";
            string word = null;
            int index = htmlString.IndexOf(failedString);
            if (index > 0)
            {
                
                for (int i = index+failedString.Length, l = htmlString.IndexOf("<", index); i < l; i++)
                {
                    word += htmlString[i];
                }
                word = word.Trim();
                this.words.Add(word.ToLower());
                this.addedWords++;
                Console.WriteLine("Spravne slovo bylo: " + word);
                log(word,3);
                return null;
            }
            else
            {
                string altPattern = @"(title="")[^""]*";

                Regex imgRgx = new Regex(altPattern, RegexOptions.IgnoreCase);
                MatchCollection matches = imgRgx.Matches(htmlString);
                int l=0;
                if (htmlString.Contains("Gratuluji! Máte slovo :-)"))
                {
                    l = matches.Count;
                }
                else
                {
                    l = matches.Count - 26;
                }
                for (int i = 0; i < l; i++)
                {
                    if (matches[i].Value.Length <= 10)
                    {
                        word += matches[i].Value.ElementAt(7);
                    }
                    else
                    {
                        word += " ";
                    }

                }
                return word;
            }

          }
        protected void serializuj()
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(this.words.GetType());
                using (StreamWriter sw = new StreamWriter("words.xml"))
                {
                    serializer.Serialize(sw, this.words);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        protected void deserializuj()
        {
            try
            {
                if (File.Exists("words.xml"))
                {
                    XmlSerializer serializer = new XmlSerializer(this.words.GetType());
                    using (StreamReader sr = new StreamReader("words.xml"))
                    {
                        this.words = (HashSet<string>)serializer.Deserialize(sr);
                    }
                }
                else throw new FileNotFoundException("Soubor nebyl nalezen");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        protected void log(string text, short mode)
        {   
            //mode 1-succes 2-success unknown word 3-failed 4-guessed letter
            switch (mode)
            {
                case 1:
                    text = DateTime.Now.ToString("HH:mm:ss") + ";SUCCESS;" + text+"\n";
                    break;
                case 2:
                    text = DateTime.Now.ToString("HH:mm:ss") + ";SUCCESSunknown;" + text+";"+this.words.Count + "\n";
                    break;
                case 3:
                    text = DateTime.Now.ToString("HH:mm:ss") + ";failed;" + text + ";" + this.words.Count +";"+this.usedLetters+ "\n";
                    break;
                case 4:
                    text = DateTime.Now.ToString("HH:mm:ss") + ";"+ text+ ";" +this.usedLetters.Last()  + ";" + this.tempWords.Count + "\n";
                    break;
            }
            
                
            
            System.IO.File.AppendAllText("hangmanLogFile.txt",text);
        }

    }
}
